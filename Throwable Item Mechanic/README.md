# Throwable Item Mechanic

This component was made for "RITE of ILK", a two player couch co-op game where you play together as two childeren
bound together by a rope.

The idea was to allow designers to quickly extend the current interactable items to allow players to throw them in the world.
The system had to be easy to set up and allow for multiple use-cases where players would be allowed to throw the item wherever they
would want in the game world. Or directly at each other without the need to aim.

### Throw Indicator Iterations
Originally, we experimented with having no visual indicator when throwing items. Eventually, this would lead to player frustration
since they would be experimenting with distances to throw the items and having their objective fail through no fault of their own because
there was no visual feedback.

The second iteration resulted in the addition of an impact indicator, so players could now clearly see where they were going to throw the items.
This seemed to be a good solution at first but resulted in some annoyances when dealing with moving objects in front of the player as the indicator
would abruptly switch to the blocking (moving) object as the player did not know exactly if they would be throwing it over or against the object.

The last iteration resulted in the addition of a impact + curve visual. Players could now clearly see when they were throwing items over a certain object
or against something. Due to the curve visual, players could now clearly see at what point a moving item in front of them would block the throwable, which
helped them in timing their throw.

#### Further Indicator Improvements

Looking back at the mechanic, the implementation of the curve drawing could be improved. Currently, a spline-mesh is drawn along the curve for the visual indication.
I would probably do this now with a particle system to allow for more (easier) custimization and to avoid having to spawn a mesh every time the player enters aim mode.