# README

Thank you for taking the time to look at my code samples. The samples contain a throwable item mechanic and a prototype of my implemenation
of the motion matching animation technology. 

Both folders contain a README with some additional explanation about the code and iterations and improvements that
were made while working on them.

*Please note that additional code samples can be provided through personal correspondance.*

Best regards,

Cairan