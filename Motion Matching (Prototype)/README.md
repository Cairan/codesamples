# Motion Matching (Animation Tech)

**[Demonstration Video](https://youtu.be/zDPF_mu3v9U)**

This is my (ongoing) personal implementation of Motion Matching in Unreal Engine 4.
My interest in Motion Matching originated from my obsession with player traversal
mechanics and the way a character would move and behave according to input given on 
a gamepad. 

The implemenation would allow me to dive into an existing game engine
and gain a better understanding of animation systems and how different elements
work together to create a final output pose. Which was something that I had wanted
to do for a while. It would also give me the opportunity to look at workflow
improvements that could help animators to quickly set up character locomotion.

*Note: My current goals of the project are to improve my understanding of Data Oriented Design to be more
cache efficient. I want to improve my understanding of SIMD instructions so that I can increase the
efficiency of the pose selection.*

*Additional note: The tech is originally inspired by [Michael Buttner's talk](https://youtu.be/z_wpgHFSWss) on Motion Matching.*

### Pose Selection
Selecting a pose was done in the traditional motion matching fashion. When the motion matching
animation node requested a new animation pose, we would loop through all available animation poses
to find the best matching one, according to different parameters supplied via gameplay. The new pose 
would then be blended with the previous poses via a blend stack to ensure transitions are smooth.

### Blend Optimization
The prototype did have a few performance issues. Using a blend-stack to blend the poses together is not 
always the most memory efficient. So currently, I am instead investigating the use of intertialization
and having the motion matching request inertialization when the output pose needs to be blended.

### Memory Optimization
Other then that I am improving the way the poses are saved within the animation database. Currently, every bone
(that is picked for pose matching) assumes that its full transform is used when comparing poses, while in reality, 
the end user might only be interested in the position, the velocity or the rotation, etc.

Since I know which data a specific pose needs when the database is compiled, I can strip out the unneccessary data
and improve the memory size and alligment of the poses within the animation database.

*Vectorized Example*<br>
Channel 1: Compare Bone Position (vector3)<br>
Channel 2: Compare Bone Vector Length (float)<br>
Channel 3: Compare Trajectory Position (vector3)<br>
Channel 4: Compare Trajectory Rotation (quat)<br>

This could be packed in alligned Vector Registers:

`{ c1, c1, c1, c2 }{ c3, c3, c3, c4 }{ c4, c4, c4, ... }`

This way I should see a improvement in file size and loading of the poses should also be more efficient.