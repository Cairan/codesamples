//==============================================================
// AnimNode_MotionMatching
// Copyright 1998-2019 Cairan Steverink. All Rights Reserved.
//==============================================================

#pragma once
//--------------------------------------------------------------

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Goal.h"
#include "AnimationFrameData.h"
#include "MotionMatchingEnums.h"
#include "MotionMatchingUtilities.generated.h"
//--------------------------------------------------------------

class UAnimationDatabase;
class UWorld;
class UAnimSequence;
//--------------------------------------------------------------

namespace FMotionMatchingUtils
{
	const TArray<float> TrajectoryIntervals = { 0.2f, 0.4f, 0.7f, 1.0f };
	
	const float MaxTime = TrajectoryIntervals.Last();

	// Buffers for the future and past trajectory
	const float FutureTrajectoryBuffer = 1.0f;
	const float PastTrajectoryBuffer = 1.0f;
	//----------------------------------------------------------
}
//--------------------------------------------------------------

USTRUCT()
struct MOTIONMATCHING_API FMotionMatchingParams
{
	GENERATED_USTRUCT_BODY()

	FMotionMatchingParams()
		: Responsiveness(1.0f)
		, BlendTime(0.2f)
		, bPoseMatching(true)
		, bHasCurrentAnimation(false)
		, CurrentVelocity(FVector::ZeroVector)
	{
	}
	//----------------------------------------------------------

	/** How responsive the MotionMatching is according to the Desired Goal */
	UPROPERTY()
	float Responsiveness;

	/** The blend time between the current and the next animation */
	UPROPERTY()
	float BlendTime;

	/** Is Pose Matching enabled for Motion Matching */
	UPROPERTY()
	bool bPoseMatching;

	UPROPERTY()
	bool bHasCurrentAnimation;

	/** The velocity of the current motion */
	UPROPERTY()
	FVector CurrentVelocity;

	/** Array containing bone positions and bone velocities */
	UPROPERTY()
	TArray<FMotionBoneData> CurrentBonesData;

	/** The axises to match the trajectory against */
	UPROPERTY()
	EAxisPosition TrajectoryPositionAxis;

	/** The axises to match the bones against */
	UPROPERTY()
	EAxisPosition BonePositionAxis;

	/** The penalty provided for a mismatch in category */
	UPROPERTY()
	float CategoryPenalty;

	/** The locomotion tags provided that we should be looking for */
	UPROPERTY()
	FGameplayTagContainer LocomotionCategories;
	//----------------------------------------------------------
};
//--------------------------------------------------------------

/**
 * Utility class containing static functions used for Motion Matching
 */
UCLASS()
class UMotionMatchingUtilities : public UObject
{
	GENERATED_UCLASS_BODY()

public:
	/** Get the animation candidate with the lowest cost */
	static void GetLowestCostAnimation(const UAnimationDatabase* AnimationDatabase, const FGoal& Goal, const FMotionMatchingParams& MotionMatchingParams, int& OutBestCandidateIndex, float& OutBestCandidateCost);

	/** Compute the jumping point candidate cost */
	static float ComputeCost(const FAnimationFrameData& CandidatePose, const FGoal& Goal, const FMotionMatchingParams& MotionMatchingParams);

	/** How much the candidate jumping position matches the current situation */
	static float ComputeCurrentCost(const FAnimationFrameData& CandidatePose, const FGoal& Goal, const FMotionMatchingParams& MotionMatchingParams);

	/** How much the candidate piece of motion matches the desired trajectory */
	static float ComputeFutureCost(const FAnimationFrameData& CandidatePose, const FGoal& Goal, const FMotionMatchingParams& MotionMatchingParams);

	/** Create a Goal and Trajectory from our current input and desired speed */
	static FGoal MakeGoal(const float DesiredSpeed, const FVector InputDirectionNormal, const FTransform CharacterMeshTM, const TArray<float> TrajectoryIntervals);

	/** Draws the current goal and trajectory */
	static void DrawDebugGoal(const UWorld* World, const FGoal& InGoal, const FTransform CharacterMeshTM);

	/** Draws the current goal and trajectory */
	static void DrawDebugDirection(const UWorld* World, const FGoal& InGoal, const FTransform CharacterMeshTM);

	/** Get a list of Bone Data from the list of matched bones */
	static TArray<struct FMotionBoneData> GetBoneDataFromAnimation(const UAnimSequence* InAnimSequence, const float InTime, const TArray<FName>& InBones);

	/** Transform a location/rotation in bone relative space to world space. */
	static FTransform GetTransformFromBoneSpace(const UAnimSequence* InAnimSequence, const float InTime, const struct FReferenceSkeleton& InReferenceSkeleton, const int InBoneIndex);
	//----------------------------------------------------------
};
//--------------------------------------------------------------

//==============================================================


