//==============================================================
// AnimNode_MotionMatching
// Copyright 1998-2019 Cairan Steverink. All Rights Reserved.
//==============================================================

#pragma once
//--------------------------------------------------------------

#include "CoreMinimal.h"
#include "AlphaBlend.h"
#include "AnimationFrameData.h"
#include "BoneContainer.h"
#include "BonePose.h"
#include "GameplayTagContainer.h"
#include "MotionMatchingSampleData.h"

#include "Animation/AnimNodeBase.h"
#include "Animation/AnimNode_AssetPlayerBase.h"

#include "UObject/ObjectMacros.h"
#include "Utilities/MotionMatchingUtilities.h"

#include "AnimNode_MotionMatching.generated.h"
//--------------------------------------------------------------

class UAnimationDatabase;
class UBlendProfile;
class UCurveFloat;
class AActor;
class UWorld;
struct FGoal;
struct FAnimationFrameData;
//--------------------------------------------------------------

USTRUCT(BlueprintInternalUseOnly)
struct MOTIONMATCHING_API FAnimNode_MotionMatching : public FAnimNode_AssetPlayerBase
{
	GENERATED_BODY()

public:
	/** The Animation Database that we will use for Motion Matching */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	UAnimationDatabase* AnimationDatabase;
	
	/** The Goal to match the Animation Data against */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	FGoal Goal;

	/** How responsive the MotionMatching is according to the Desired Goal */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	float Responsiveness;

	/** The blend time between the current and the next animation */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	float BlendTime;

	/** The penalty provided (per category) to a candidate pose when it does not match a provided category */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	float CategoryMismatchPenaly;

	/** Which categories to match the animation against. This can be multiple */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinShownByDefault))
	FGameplayTagContainer ActiveLocomotionCategories;

	/** Should we match the position of the bones for realistic movement */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinHiddenByDefault))
	bool bEnablePoseMatching;

	/** Which axises we want to use to match the trajectory positions */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinHiddenByDefault))
	EAxisPosition TrajectoryPositionAxis;

	/** Which axises we want to use to match the bone positions */
	UPROPERTY(Category = Config, EditAnywhere, BlueprintReadWrite, meta = (PinHiddenByDefault))
	EAxisPosition BonePositionAxis;

	UPROPERTY(Category = BlendType, EditAnywhere)
	EAlphaBlendOption BlendType;

	UPROPERTY(Category = BlendType, EditAnywhere)
	UCurveFloat* CustomBlendCurve;
	//----------------------------------------------------------

protected:
	UPROPERTY()
	TArray<FMotionMatchingSampleData> AnimationSamples;

	UPROPERTY()
	FMotionMatchingSampleData LastActiveChildSample;

	TArray<FMotionMatchingSampleData> SamplesToEvaluate;
	//----------------------------------------------------------

public:
	FAnimNode_MotionMatching();
	//----------------------------------------------------------

	virtual float GetCurrentAssetTime() override;
	virtual float GetCurrentAssetTimePlayRateAdjusted() override;
	virtual float GetCurrentAssetLength() override;

	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void UpdateAssetPlayer(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext & Output) override;
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;

	virtual void UpdateMotionMatching( const FMotionMatchingParams& MotionMatchingParams, const FPoseContext& Output );
	//----------------------------------------------------------

protected:
	void UpdateAnimationSampleData(const FAnimationUpdateContext& Context);
	void EvaluateBlendPose(FPoseContext& Output);
	void SetCurrentAnimation( const int InAnimationIndex, const float InTime );
	//----------------------------------------------------------
	
	UAnimSequence* GetCurrentAnim();
	//----------------------------------------------------------
};
//--------------------------------------------------------------

//==============================================================

